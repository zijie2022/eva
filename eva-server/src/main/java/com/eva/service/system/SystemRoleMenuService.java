package com.eva.service.system;

import com.eva.core.model.PageData;
import com.eva.core.model.PageWrap;
import com.eva.dao.system.model.SystemRoleMenu;
import java.util.List;

/**
 * 角色菜单关联Service定义
 * @author Eva.Caesar Liu
 * @date 2021/08/06 22:58
 */
public interface SystemRoleMenuService {

    /**
     * 创建
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    Integer create(SystemRoleMenu systemRoleMenu);

    /**
     * 主键删除
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    void deleteById(Integer id);

    /**
     * 删除
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    void delete(SystemRoleMenu systemRoleMenu);

    /**
     * 批量主键删除
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    void updateById(SystemRoleMenu systemRoleMenu);

    /**
     * 批量主键更新
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    void updateByIdInBatch(List<SystemRoleMenu> systemRoleMenus);

    /**
     * 主键查询
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    SystemRoleMenu findById(Integer id);

    /**
     * 条件查询单条记录
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    SystemRoleMenu findOne(SystemRoleMenu systemRoleMenu);

    /**
     * 条件查询
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    List<SystemRoleMenu> findList(SystemRoleMenu systemRoleMenu);
  
    /**
     * 分页查询
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    PageData<SystemRoleMenu> findPage(PageWrap<SystemRoleMenu> pageWrap);

    /**
     * 条件统计
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    long count(SystemRoleMenu systemRoleMenu);
}
