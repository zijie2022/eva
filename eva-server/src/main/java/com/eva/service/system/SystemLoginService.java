package com.eva.service.system;

import com.eva.dao.system.dto.LoginDTO;

import javax.servlet.http.HttpServletRequest;

/**
 * 系统登录
 * @author Eva.Caesar Liu
 * @date 2021/08/06 22:58
 */
public interface SystemLoginService {

    /**
     * 密码登录
     * @author Eva.Caesar Liu
     * @date 2021/08/06 22:58
     */
    String loginByPassword (LoginDTO dto, HttpServletRequest request);
}
